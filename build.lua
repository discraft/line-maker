function checkFuel()
    if turtle.getFuelLevel() < 32 then
        turtle.select(1)
        turtle.placeUp()
        turtle.suckUp(8)
        turtle.refuel()
        turtle.digUp()
    end
end

function checkItemCount()
    if turtle.getItemCount() == 1 then
        turtle.select(2)
        turtle.placeUp()
        turtle.select(3)
        turtle.suckUp(63)
        turtle.select(2)
        turtle.digUp()
        turtle.select(3)
    end
end

checkFuel()

while turtle.forward() do
    checkFuel()

    turtle.select(3)
    checkItemCount()
    turtle.placeDown()
end
